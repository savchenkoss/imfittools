import numpy as np

"""
A bunch of functions to fix imfit model functions: make sure that all parameters
are in good regions, have limits etc.
"""


def fix_Sersic(func):
    """
    Function checks all parameters of a sersic function, makes sure that
    they are all have reasonable values, set ranges if they are missing
    """
    if func.own_coords:
        x0 = func.get_par_by_name("X0")
        if (not x0.fixed) and (x0.lowerLim is None):
            # PA ranges are missing, let's add some
            x0.lowerLim = x0.value - 5
            x0.upperLim = x0.value + 5
        y0 = func.get_par_by_name("Y0")
        if (not y0.fixed) and (y0.lowerLim is None):
            # PA ranges are missing, let's add some
            y0.lowerLim = y0.value - 5
            y0.upperLim = y0.value + 5
    pa = func.get_par_by_name("PA")
    if (not pa.fixed) and (pa.lowerLim is None):
        # PA ranges are missing, let's add some
        pa.lowerLim = pa.value - 10
        pa.upperLim = pa.value + 10
    ell = func.get_par_by_name("ell")
    if (not ell.fixed) and (ell.lowerLim is None):
        # Ell ranges are missing, let's add some
        ell.lowerLim = 0
        ell.upperLim = 0.25
    n = func.get_par_by_name("n")
    if (not n.fixed) and (n.lowerLim is None):
        # Ell ranges are missing, let's add some
        n.lowerLim = 0.333
        n.upperLim = 8.0
    i_e = func.get_par_by_name("I_e")
    if (not i_e.fixed) and (i_e.lowerLim is None):
        # I_e ranges are missing, let's add some
        i_e.lowerLim = 1
        i_e.upperLim = 15000
    if i_e.value < 1:
        i_e.value = 5
    if i_e.value > 15000:
        i_e.value = 12000
    r_e = func.get_par_by_name("r_e")
    if r_e.value < 1:
        r_e.value = 2
    if r_e.value > 40:
        i_e.value = 30
    if (not r_e.fixed) and (r_e.lowerLim is None):
        # I_e ranges are missing, let's add some
        r_e.lowerLim = min(1, r_e.value)
        r_e.upperLim = max(1.25 * r_e.value, 40)


def fix_ExponentialDisk3D(func):
    if func.own_coords:
        x0 = func.get_par_by_name("X0")
        if (not x0.fixed) and (x0.lowerLim is None):
            # PA ranges are missing, let's add some
            x0.lowerLim = x0.value - 5
            x0.upperLim = x0.value + 5
        y0 = func.get_par_by_name("Y0")
        if (not y0.fixed) and (y0.lowerLim is None):
            # PA ranges are missing, let's add some
            y0.lowerLim = y0.value - 5
            y0.upperLim = y0.value + 5
    pa = func.get_par_by_name("PA")
    if (not pa.fixed) and (pa.lowerLim is None):
        # PA ranges are missing, let's add some
        pa.lowerLim = pa.value - 10
        pa.upperLim = pa.value + 10
    inc = func.get_par_by_name("inc")
    if (not inc.fixed) and (inc.lowerLim is None):
        # inc ranges are missing, let's add some
        inc.lowerLim = max(0, inc.value - 10)
        inc.upperLim = min(inc.value + 10, 90)
    j_0 = func.get_par_by_name("J_0")
    if (not j_0.fixed) and (j_0.lowerLim is None):
        j_0.lowerLim = 1
        j_0.upperLim = 1e5
    if j_0.value < 1:
        j_0.value = 2
    if j_0.value > 1e5:
        j_0.value = 8e4
    h = func.get_par_by_name("h")
    if (not h.fixed) and (h.lowerLim is None):
        h.lowerLim = 2.5
        h.upperLim = np.inf
    if h.value < 5:
        h.value = 6
    if h.value > 1000:
        h.value = 200
    n = func.get_par_by_name("n")
    if (not n.fixed) and (n.lowerLim is None):
        n.lowerLim = 0.5
        n.upperLim = 20
    z_0 = func.get_par_by_name("z_0")
    if (z_0.value < 1.0) or np.isnan(z_0.value):
        z_0.value = 1.0
    if (not z_0.fixed) and (z_0.lowerLim is None):
        z_0.lowerLim = 1
        z_0.upperLim = np.inf


def fix_BknExp3D(func):
    if func.own_coords:
        x0 = func.get_par_by_name("X0")
        if (not x0.fixed) and (x0.lowerLim is None):
            # PA ranges are missing, let's add some
            x0.lowerLim = x0.value - 5
            x0.upperLim = x0.value + 5
        y0 = func.get_par_by_name("Y0")
        if (not y0.fixed) and (y0.lowerLim is None):
            # PA ranges are missing, let's add some
            y0.lowerLim = y0.value - 5
            y0.upperLim = y0.value + 5
    pa = func.get_par_by_name("PA")
    if (not pa.fixed) and (pa.lowerLim is None):
        # PA ranges are missing, let's add some
        pa.lowerLim = pa.value - 10
        pa.upperLim = pa.value + 10
    inc = func.get_par_by_name("inc")
    if (not inc.fixed) and (inc.lowerLim is None):
        # inc ranges are missing, let's add some
        inc.lowerLim = max(0, inc.value - 10)
        inc.upperLim = min(inc.value + 10, 90)
    j_0 = func.get_par_by_name("J_0")
    if (not j_0.fixed) and (j_0.lowerLim is None):
        j_0.lowerLim = 1
        j_0.upperLim = 1e5
    if j_0.value < 1:
        j_0.value = 2
    if j_0.value > 1e5:
        j_0.value = 8e4
    h1 = func.get_par_by_name("h1")
    if (not h1.fixed) and (h1.lowerLim is None):
        h1.lowerLim = 2.5
        h1.upperLim = np.inf
    if h1.value < 5:
        h1.value = 6
    if h1.value > 1000:
        h1.value = 900
    h2 = func.get_par_by_name("h2")
    if (not h2.fixed) and (h2.lowerLim is None):
        h2.lowerLim = 2.5
        h2.upperLim = np.inf
    if h2.value < 5:
        h2.value = 6
    if h2.value > 1000:
        h2.value = 900
    r_break = func.get_par_by_name("r_break")
    if (not r_break.fixed) and (r_break.lowerLim is None):
        r_break.lowerLim = 5
        r_break.upperLim = np.inf
    if r_break.value < 5:
        r_break.value = 5
    n = func.get_par_by_name("n")
    if (not n.fixed) and (n.lowerLim is None):
        n.lowerLim = 0.5
        n.upperLim = 20
    z_0 = func.get_par_by_name("z_0")
    if (z_0.value < 1.0) or np.isnan(z_0.value):
        z_0.value = 1.0
    if (not z_0.fixed) and (z_0.lowerLim is None):
        z_0.lowerLim = 1
        z_0.upperLim = np.inf


def fix_DblBknExp3D(func):
    if func.own_coords:
        x0 = func.get_par_by_name("X0")
        if (not x0.fixed) and (x0.lowerLim is None):
            # PA ranges are missing, let's add some
            x0.lowerLim = x0.value - 5
            x0.upperLim = x0.value + 5
        y0 = func.get_par_by_name("Y0")
        if (not y0.fixed) and (y0.lowerLim is None):
            # PA ranges are missing, let's add some
            y0.lowerLim = y0.value - 5
            y0.upperLim = y0.value + 5
    pa = func.get_par_by_name("PA")
    if (not pa.fixed) and (pa.lowerLim is None):
        # PA ranges are missing, let's add some
        pa.lowerLim = pa.value - 10
        pa.upperLim = pa.value + 10
    inc = func.get_par_by_name("inc")
    if (not inc.fixed) and (inc.lowerLim is None):
        # inc ranges are missing, let's add some
        inc.lowerLim = max(0, inc.value - 10)
        inc.upperLim = min(inc.value + 10, 90)
    j_0 = func.get_par_by_name("J_0")
    if (not j_0.fixed) and (j_0.lowerLim is None):
        j_0.lowerLim = 1
        j_0.upperLim = 1e5
    if j_0.value < 1:
        j_0.value = 2
    if j_0.value > 1e5:
        j_0.value = 8e4
    h1 = func.get_par_by_name("h1")
    if (not h1.fixed) and (h1.lowerLim is None):
        h1.lowerLim = 2.5
        h1.upperLim = np.inf
    if h1.value < 5:
        h1.value = 6
    if h1.value > 1000:
        h1.value = 900
    h2 = func.get_par_by_name("h2")
    if (not h2.fixed) and (h2.lowerLim is None):
        h2.lowerLim = 2.5
        h2.upperLim = np.inf
    if h2.value < 5:
        h2.value = 6
    if h2.value > 1000:
        h2.value = 900
    h3 = func.get_par_by_name("h3")
    if (not h3.fixed) and (h3.lowerLim is None):
        h3.lowerLim = 2.5
        h3.upperLim = np.inf
    if h3.value < 5:
        h3.value = 6
    if h3.value > 1000:
        h3.value = 900
    r_break1 = func.get_par_by_name("r_break1")
    if (not r_break1.fixed) and (r_break1.lowerLim is None):
        r_break1.lowerLim = 5
        r_break1.upperLim = np.inf
    if r_break1.value < 5:
        r_break1.value = 5
    r_break2 = func.get_par_by_name("r_break2")
    if (not r_break2.fixed) and (r_break2.lowerLim is None):
        r_break2.lowerLim = 5
        r_break2.upperLim = np.inf
    if r_break2.value < 5:
        r_break2.value = 5
    n = func.get_par_by_name("n")
    if (not n.fixed) and (n.lowerLim is None):
        n.lowerLim = 0.5
        n.upperLim = 20
    z_0 = func.get_par_by_name("z_0")
    if (z_0.value < 1.0) or np.isnan(z_0.value):
        z_0.value = 2.0
    if (not z_0.fixed) and (z_0.lowerLim is None):
        z_0.lowerLim = 1
        z_0.upperLim = np.inf


fixers_list = {"Sersic": fix_Sersic, "ExponentialDisk3D": fix_ExponentialDisk3D,
               "BknExp3D": fix_BknExp3D, "DblBknExp3D": fix_DblBknExp3D}
