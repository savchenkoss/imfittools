#!/usr/bin/env python

import argparse
import subprocess
from pathlib import Path
from os import makedirs
import json
from shutil import rmtree
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from scipy.ndimage import rotate
from scipy.interpolate import interp1d
from .ImfitModel import ImfitModel


def rotate_image_and_origin(image, xy, angle):
    image[np.isnan(image)] = 0
    im_rot = rotate(image, angle, reshape=False, order=1)
    org_center = (np.array(image.shape[:2][::-1])-1)/2.
    rot_center = (np.array(im_rot.shape[:2][::-1])-1)/2.
    org = xy - org_center
    a = np.deg2rad(angle)
    new = np.array([org[0]*np.cos(a) + org[1]*np.sin(a),
                    -org[0]*np.sin(a) + org[1]*np.cos(a)])
    return im_rot, new + rot_center


def to_mag(flux, pix_scale, magzpt):
    try:
        iter(flux)
    except TypeError:
        if flux <= 0:
            flux = 0.00001
    else:
        flux[flux <= 0] = 0.00001
    return -2.5*np.log10(flux/pix_scale**2) + magzpt


def get_slice(data, x_cen, y_cen, width, kind):
    """
    Kind = 'vertical' or 'horizonthal'
    """
    if kind == "horizonthal":
        image_slice = np.median(data[int(y_cen-width): int(y_cen+width), :], axis=0)
        image_slice_std = 3*np.std(data[int(y_cen-width): int(y_cen+width), :], axis=0)
    elif kind == "vertical":
        image_slice = np.median(data[:, int(x_cen-width): int(x_cen+width)], axis=1)
        image_slice_std = 3*np.std(data[:, int(x_cen-width): int(x_cen+width)], axis=1)
    else:
        print(f"Kind can be only 'horizonthal' or 'vertical', not {kind}")
        raise ValueError
    return image_slice, image_slice_std


def get_slice_coords(data, x_cen, y_cen, kind):
    if kind == "horizonthal":
        n = data.shape[1]
        slice_x = np.arange(n)
        slice_y = np.ones_like(slice_x) * x_cen
    elif kind == "vertical":
        n = data.shape[0]
        slice_y = np.arange(n)
        slice_x = np.ones_like(slice_y) * y_cen
    else:
        print(f"Kind can be only 'horizonthal' or 'vertical', not {kind}")
        raise ValueError
    return slice_x, slice_y


def make_slice(image, imfit, mask=None, psf=None, magzpt=None, scale=None, maglim=25,
               outname="slice.png", save_data=None, x_cen=None, y_cen=None, posang=None):
    # Load imfit model
    imfit_model = ImfitModel(imfit)

    # Find the biggest components
    biggest_component = imfit_model.get_biggest_component()
    print(f"Biggest component of a model is {biggest_component.uname}")
    if (x_cen is None) or (y_cen is None):
        # Find coordinates of the biggest component as the model center
        x_cen_biggest, y_cen_biggest = biggest_component.get_center()
    if x_cen is None:
        # If no x_cen given, use one of a biggest component
        x_cen = x_cen_biggest
    if y_cen is None:
        # If no y_cen given, use one of a biggest component
        y_cen = y_cen_biggest
    # Use position angle of the biggest component as the model position angle
    print(f"It's coordinates {x_cen:1.1f},{y_cen:1.1f}.")
    if posang is None:
        posang = biggest_component.get_par_by_name("PA").value
        print(f"It's position angle {posang}")
    else:
        print(f"Using user-specified posang {posang}")

    workdir = Path("/tmp/workdir_make_slice")
    if workdir.exists():
        rmtree(workdir)
    makedirs(workdir)

    # Run makeimage to make an image
    print("Making model")
    call_str = f"makeimage {imfit} --output-functions {workdir}/comp "
    call_str += f"-o {workdir}/model.fits "
    call_str += f"--refimage {image} "
    if psf is not None:
        call_str += f"--psf {psf} "
    call_str += " >>/dev/null"
    subprocess.call(call_str, shell=True)

    # Try to read magnitude zeropoint if neccessary
    if magzpt is None:
        header = fits.getheader(image)
        for kw in ("M0", "MAGZPT"):
            if kw in header:
                magzpt = header[kw]
                print(f"Magnitude zero point found in header: {magzpt: 1.2f}")
                break
        else:
            print("Magnitude zero point was not found in the header. Please")
            print("provide it with --magzpt keyword")
            exit(1)

    # Try to read image scale from the header if neccessary
    if scale is None:
        header = fits.getheader(image)
        for kw in ("CDELT1", "CD1_1", "CD1_1", "PC1_1"):
            if "kw" in header:
                scale = header[kw]*3600
                print(f"Image scale {scale:1.2f} ''/pix found in header")
                break
        else:
            print("Image scale was not found in the header. Please")
            print("provide it with --scale keyword")
            exit(1)

    # Load rotated versions of all images
    # Object image
    image_data = fits.getdata(image)
    image_data, (x_cen_rot, y_cen_rot) = rotate_image_and_origin(image_data, xy=(x_cen, y_cen), angle=posang-90)
    # Model image
    model_data = fits.getdata(f"{workdir}/model.fits")
    model_data, _ = rotate_image_and_origin(model_data, xy=(x_cen, y_cen), angle=posang-90)
    # Mask image
    if mask is not None:
        mask_data = fits.getdata(mask)
        mask_data, _ = rotate_image_and_origin(mask_data, xy=(x_cen, y_cen), angle=posang-90)
    else:
        mask_data = np.zeros_like(image_data)
    # Components
    components = {}
    for i in range(1, len(imfit_model.listOfFunctions)+1):
        fname = list(workdir.glob(f"comp{i}*.fits"))[0]
        comp_name = fname.name[4:-5]
        comp_data = fits.getdata(fname)
        components[comp_name], _ = rotate_image_and_origin(comp_data, xy=(x_cen, y_cen), angle=posang-90)
    # Compute residuals
    # diff_data = image_data - model_data

    if save_data is not None:
        out_data = {}

    # Make a plot
    slice_width = 3
    plt.figure(figsize=(10, 5))
    # plot slices along major axis
    plt.subplot(121)
    plt.title("Major axis")
    image_slice, image_slice_std = get_slice(image_data, x_cen_rot, y_cen_rot, width=slice_width, kind="horizonthal")
    mask_slice, _ = get_slice(mask_data, x_cen_rot, y_cen_rot, width=slice_width, kind="horizonthal")
    masked_inds = np.where(mask_slice > 0.5)
    if save_data is not None:
        out_data["masked_inds_major"] = masked_inds[0].tolist()
    not_masked_inds = np.where(mask_slice < 0.5)
    full_model_slice, _ = get_slice(model_data, x_cen_rot, y_cen_rot, width=slice_width, kind="horizonthal")
    # Plot original image slice
    good_inds = np.where(to_mag(full_model_slice, scale, magzpt) < maglim)
    radius = scale * (np.arange(len(image_slice)) - x_cen_rot)
    x = np.copy(radius)
    y1 = to_mag(image_slice-image_slice_std, scale, magzpt)
    y2 = to_mag(image_slice+image_slice_std, scale, magzpt)
    x[masked_inds] = np.nan
    y1[masked_inds] = np.nan
    y2[masked_inds] = np.nan
    plt.fill_between(x=x[good_inds], y1=y1[good_inds], y2=y2[good_inds], color="0.75")
    if save_data is not None:
        out_data["dist_major"] = x.tolist()
        y = (y1 - y2) / 2
        out_data["mu_observed_std_major"] = y.tolist()
    y = to_mag(image_slice, scale, magzpt)
    x[masked_inds] = np.nan  # Take a mask into account
    y[masked_inds] = np.nan  #
    plt.plot(x[good_inds], y[good_inds], label="Image", color="C0")  # Plot masked data
    if save_data is not None:
        out_data["mu_observed_major"] = y.tolist()
    # Fill the masked gaps
    x_not_masked = np.copy(radius)[not_masked_inds]
    y = to_mag(image_slice, scale, magzpt)[not_masked_inds]
    y1 = to_mag(image_slice-image_slice_std, scale, magzpt)[not_masked_inds]
    y2 = to_mag(image_slice+image_slice_std, scale, magzpt)[not_masked_inds]
    y_interp = interp1d(x=x_not_masked, y=y, bounds_error=False, fill_value='extrapolate')
    y1_interp = interp1d(x=x_not_masked, y=y1, bounds_error=False, fill_value='extrapolate')
    y2_interp = interp1d(x=x_not_masked, y=y2, bounds_error=False, fill_value='extrapolate')
    x = np.copy(radius)
    y_masked = y_interp(x)
    y1_masked = y1_interp(x)
    y2_masked = y2_interp(x)
    x[not_masked_inds] = np.nan
    y_masked[not_masked_inds] = np.nan
    y1_masked[not_masked_inds] = np.nan
    y2_masked[not_masked_inds] = np.nan
    plt.plot(x[good_inds], y_masked[good_inds], linestyle=":", color="C0")  # Fill masked gaps
    plt.fill_between(x=x[good_inds], y1=y1_masked[good_inds], y2=y2_masked[good_inds], color="0.75")
    # Plot full model slice
    y = to_mag(full_model_slice, scale, magzpt)
    plt.plot(radius[good_inds], y[good_inds], label="Model", color="C1")
    if save_data is not None:
        out_data["mu_full_model_major"] = y.tolist()
    # Plot components
    for idx, comp_name in enumerate(components.keys()):
        color_idx = (idx + 2) % 10
        comp_slice, _ = get_slice(components[comp_name], x_cen_rot, y_cen_rot, width=slice_width, kind="horizonthal")
        y = to_mag(comp_slice, scale, magzpt)
        if save_data is not None:
            out_data[f"mu_{comp_name}_major"] = y.tolist()
        plt.plot(radius[good_inds], y[good_inds], label=comp_name[2:], color=f"C{color_idx}")
    plt.ylim([maglim, np.nanmin(to_mag(image_slice, scale, magzpt))-0.5])
    if len(radius[good_inds]) > 2:
        plt.xlim([radius[good_inds][0], radius[good_inds][-1]])
    plt.xlabel("radius [arcsec]")
    plt.ylabel("$\mu$ [mag/sq.arcsec]")
    plt.legend()

    # Plot slice along minor axis
    plt.subplot(122)
    plt.title("Minor axis")
    image_slice, image_slice_std = get_slice(image_data, x_cen_rot, y_cen_rot, width=slice_width, kind="vertical")
    full_model_slice, _ = get_slice(model_data, x_cen_rot, y_cen_rot, width=slice_width, kind="vertical")
    mask_slice, _ = get_slice(mask_data, x_cen_rot, y_cen_rot, width=slice_width, kind="vertical")
    masked_inds = np.where(mask_slice > 0.5)
    if save_data is not None:
        out_data["masked_inds_minor"] = masked_inds[0].tolist()
    not_masked_inds = np.where(mask_slice < 0.5)
    # Plot original image slice
    good_inds = np.where(to_mag(full_model_slice, scale, magzpt) < maglim)
    radius = scale * (np.arange(len(image_slice)) - y_cen_rot)
    x = np.copy(radius)
    y1 = to_mag(image_slice-image_slice_std, scale, magzpt)
    y2 = to_mag(image_slice+image_slice_std, scale, magzpt)
    x[masked_inds] = np.nan
    y1[masked_inds] = np.nan
    y2[masked_inds] = np.nan
    plt.fill_between(x=x[good_inds], y1=y1[good_inds], y2=y2[good_inds], color="0.75")
    if save_data is not None:
        out_data["dist_minor"] = x.tolist()
        y = (y1 - y2) / 2
        out_data["mu_observed_std_minor"] = y.tolist()
    y = to_mag(image_slice, scale, magzpt)
    x[masked_inds] = np.nan  # Take a mask into account
    y[masked_inds] = np.nan  #
    plt.plot(x[good_inds], y[good_inds], label="Image", color="C0")  # Plot masked data
    if save_data is not None:
        out_data["mu_observed_minor"] = y.tolist()
    # Fill the masked gaps
    x_not_masked = np.copy(radius)[not_masked_inds]
    y = to_mag(image_slice, scale, magzpt)[not_masked_inds]
    y1 = to_mag(image_slice-image_slice_std, scale, magzpt)[not_masked_inds]
    y2 = to_mag(image_slice+image_slice_std, scale, magzpt)[not_masked_inds]
    y_interp = interp1d(x=x_not_masked, y=y, bounds_error=False, fill_value='extrapolate')
    y1_interp = interp1d(x=x_not_masked, y=y1, bounds_error=False, fill_value='extrapolate')
    y2_interp = interp1d(x=x_not_masked, y=y2, bounds_error=False, fill_value='extrapolate')
    x = np.copy(radius)
    y_masked = y_interp(x)
    y1_masked = y1_interp(x)
    y2_masked = y2_interp(x)
    x[not_masked_inds] = np.nan
    y_masked[not_masked_inds] = np.nan
    y1_masked[not_masked_inds] = np.nan
    y2_masked[not_masked_inds] = np.nan
    plt.plot(x[good_inds], y_masked[good_inds], linestyle=":", color="C0")  # Fill masked gaps
    plt.fill_between(x=x[good_inds], y1=y1_masked[good_inds], y2=y2_masked[good_inds], color="0.75")
    # plot full model slice
    y = to_mag(full_model_slice, scale, magzpt)
    plt.plot(radius[good_inds], y[good_inds], label="Model", color="C1")
    if save_data is not None:
        out_data["mu_full_model_minor"] = y.tolist()
    # Plot components
    for idx, comp_name in enumerate(components.keys()):
        color_idx = (idx+2) % 10
        comp_slice, _ = get_slice(components[comp_name], x_cen_rot, y_cen_rot, width=slice_width, kind="vertical")
        y = to_mag(comp_slice, scale, magzpt)
        plt.plot(radius[good_inds], y[good_inds], label=comp_name[2:], color=f"C{color_idx}")
        if save_data is not None:
            out_data[f"mu_{comp_name}_minor"] = y.tolist()
    if len(radius[good_inds]) > 2:
        plt.xlim([radius[good_inds][0], radius[good_inds][-1]])
    plt.ylim([maglim, np.nanmin(to_mag(image_slice, scale, magzpt))-0.5])
    plt.xlabel("radius [arcsec]")
    plt.ylabel("$\mu$ [mag/sq.arcsec]")
    plt.tight_layout()
    if outname == "show":
        plt.show()
    else:
        if Path(outname).name.endswith("png"):
            plt.savefig(outname, dpi=150)
        else:
            plt.savefig(outname)
    if save_data is not None:
        json.dump(out_data, open(save_data, "w"))
    if workdir.exists():
        rmtree(workdir)


def find_iso_radius(imfit, magzpt, scale, maglim, psf=None):
    """
    Finds a radius at which the model flux along major axis goes below a given magnitude limit.
    Returns the value in arc seconds
    """
    imfit_model = ImfitModel(imfit)
    # Find the center and orientation of the biggest component
    biggest_component = imfit_model.get_biggest_component()
    x_cen, y_cen = biggest_component.get_center()
    posang = biggest_component.get_par_by_name("PA").value

    workdir = Path("/tmp/workdir_make_slice")
    if workdir.exists():
        rmtree(workdir)
    makedirs(workdir)

    # Make an image twise as big as given coordinates
    call_str = f"makeimage {imfit} -o {workdir}/model_iso.fits "
    call_str += f"-ncols {int(x_cen*2)} -nrows {int(y_cen*2)}"
    if psf is not None:
        call_str += f"--psf {psf} "
    call_str += " >>/dev/null"
    subprocess.call(call_str, shell=True)

    # Load model data
    model_data = fits.getdata(f"{workdir}/model_iso.fits")
    model_data, (x_cen_rot, y_cen_rot) = rotate_image_and_origin(model_data, xy=(x_cen, y_cen), angle=posang-90)

    # Make a slice
    model_slice, _ = get_slice(model_data, x_cen_rot, y_cen_rot, width=2, kind="horizonthal")
    max_idx = np.argmax(model_slice)
    for idx in range(max_idx, len(model_slice)):
        mag = to_mag(model_slice[idx], scale, magzpt)
        if mag > maglim:
            return (idx-max_idx) * scale
    print("Warning (find_iso_radius): the image edge was reached. Found value can be underestimated")
    return (idx-max_idx) * scale


def make_imfit_slice():
    parser = argparse.ArgumentParser()
    parser.add_argument("--image", help="Original galaxy image")
    parser.add_argument("--imfit", help="Imfit config with model")
    parser.add_argument("--mask", help="Mask image")
    parser.add_argument("--psf", help="PSF image [optional]")
    parser.add_argument("--x_cen", help="Center x-coordinate. Default: biggest component's center",
                        type=float, default=None)
    parser.add_argument("--y_cen", help="Center y-coordinate. Default: biggest component's center",
                        type=float, default=None)
    parser.add_argument("--posang", help="Position angle of a slice. Default: biggest component's major and minor axis",
                        type=float, default=None)
    parser.add_argument("--magzpt", help="Magnitude zeropoint. If not given a value from FITS header will be used",
                        type=float)
    parser.add_argument("--scale", help="Image scale ''/pix. If not given a value from FITS header will be used",
                        type=float)
    parser.add_argument("--maglim", help="Slice lower limit in magnitudes per sq.arcsec, default=25",
                        type=float, default=25)
    parser.add_argument("--outname", help="Name of the output file, default slice.png", default="slice.png")
    parser.add_argument("--save-data", help="Output file name for a json file to save slice")
    args = parser.parse_args()
    make_slice(image=args.image, imfit=args.imfit, mask=args.mask, psf=args.psf,
               magzpt=args.magzpt, scale=args.scale, maglim=args.maglim,
               outname=args.outname, x_cen=args.x_cen, y_cen=args.y_cen, posang=args.posang,
               save_data=args.save_data)
