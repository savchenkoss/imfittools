#! /usr/bin/env python

import argparse
from .ImfitModel import ImfitModel


def merge_inputs(inputFile, bestFitFile, mergedFile):
    inputModel = ImfitModel(inputFile)
    bestFitModel = ImfitModel(bestFitFile)
    bestFitModel.copy_boundaries(inputModel)
    bestFitModel.copy_add_params(inputModel)
    bestFitModel.create_input_file(mergedFile, fixAll=False)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("params", help="Imfit config file with parameters")
    parser.add_argument("bounds", help="Imfit config file with bounds")
    parser.add_argument("output", help="Output imfit config")
    args = parser.parse_args()
    merge_inputs(args.params, args.bounds, args.output)
