#!/usr/bin/env python


# define sane ranges for parameetrs
param_ranges = {
    "BknEdgeOn": {
        "PA": (-360, 360),
        "J_0": (0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "r_break": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.5, None)
    },
    "BknExp3D": {
        "PA": (-360, 360),
        "inc": (0, 180),
        "J_0": (0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "r_break": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.1, None)
    },
    "BrokenExponential": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0.0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "r_break": (0.1, None),
        "alpha": (0.1, None)
    },
    "BrokenExponential2": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0.1, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "r_break": (0.1, None)
    },
    "BrokenExponential2D": {
        "PA": (-360, 360),
        "I_0": (0.0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "r_break": (0.1, None),
        "alpha": (0.1, None),
        "h_z": (0.1, None)
    },
    "BrokenExponentialDisk3D": {
        "PA": (-360, 360),
        "inc": (0, 180),
        "J_0": (0.0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "r_break": (0.1, None),
        "alpha": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.1, None)
    },
    "Core-Sersic": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "n": (0.3, 10),
        "I_b": (0, None),
        "r_e": (0.1, None),
        "r_b": (0.1, None),
        "alpha": (0.1, 100),
        "gamma": (0.1, 10)
    },
    "DblBknEdgeOn": {
        "PA": (-360, 360),
        "J_0": (0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "h3": (0.1, None),
        "r_break_1": (0.1, None),
        "r_break_2": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.1, None)
    },
    "DblBknExp3D": {
        "PA": (-360, 360),
        "inc": (0, 180),
        "J_0": (0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "h3": (0.1, None),
        "r_break1": (0.1, None),
        "r_break2": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.1, None)
    },
    "DoubleBrokenExponential": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0, None),
        "h1": (0.1, None),
        "h2": (0.1, None),
        "h3": (0.1, None),
        "r_break1": (0.1, None),
        "r_break2": (0.1, None)
    },
    "DustyExp3DGalaxy": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "n_bulge": (0.3, 10),
        "I_e": (0, None),
        "r_e": (0.1, None),
        "inc": (0, 180),
        "J_0": (0, None),
        "h": (0.1, None),
        "n_disk": (0.5, 20),
        "z_0": (0.1, None),
        "J_0_dust": (0, None),
        "h_dust": (0.1, None),
        "n_dust": (0.5, 20),
        "z_0_dust": (0.1, None)
    },
    "EdgeOnDisk": {
        "PA": (-360, 360),
        "L_0": (0, None),
        "h": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.1, None)
    },
    "EdgeOnRing": {
        "PA": (-360, 360),
        "I_0": (0, None),
        "r": (0.1, None),
        "sigma_r": (0.1, None),
        "sigma_z": (0.1, "None")
    },
    "EdgeOnRing2Side": {
        "PA": (-360, 360),
        "I_0": (0, None),
        "r": (0.1, None),
        "sigma_r_in": (0.1, None),
        "sigma_r_out": (0.1, None),
        "sigma_z": (0.1, None),
    },
    "Exponential": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0, None),
        "h": (0.1, None)
    },
    "ExponentialDisk3D": {
        "PA": (-360, 360),
        "inc": (0, 180),
        "J_0": (0, None),
        "h": (0.1, None),
        "n": (0.5, 20),
        "z_0": (0.1, None)
    },
    "Exponential_GenEllipse": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "c0": (-4, 4),
        "I_0": (0, None),
        "h": (0.1, None)
    },
    "FlatSky": {
        "I_sky": (None, None)
    },
    "Gaussian": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0, None),
        "sigma": (0.001, None)
    },
    "GaussianJet": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0, None),
        "sigma": (0.001, None)
    },
    "GaussianRing": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "A": (0.1, None),
        "R_ring": (0.1, None),
        "sigma_r": (0.1, None),
    },
    "Moffat": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "I_0": (0, None),
        "fwhm": (0.1, None),
        "beta": (0.1, None),
    },
    "PointSource": {
        "I_tot": (0, None)
    },
    "Sersic": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "n": (0.3, 10),
        "I_e": (0, None),
        "r_e": (0.1, None),
    },
    "Sersic_GenEllipse": {
        "PA": (-360, 360),
        "ell": (0, 1),
        "c0": (-4, 4),
        "n": (0.3, 10),
        "I_e": (0, None),
        "r_e": (0.1, None)
    }
}

# SpiralBranch
# m0
# m1
# m2
# m3
# PA
# inc
# fi0
# r0
# fi_max
# n
# is_clockwise
# I0
# r_e
# width_i
# outer_r_e
# inner_r_e
# fi_of_max
# outer_n
# inner_n

# tiltedskyplane
# I_0
# m_x
# m_y

# FerrersBar2D
# PA
# ell
# c0
# n
# I_0
# a_bar

# FerrersBar3D
# PA
# inc
# barPA
# J_0
# R_bar
# q
# q_z
# n

# FlatBar
# PA
# ell
# deltaPA_max
# I_0
# h1
# h2
# r_break
# alpha

# GaussianRing2Side
# PA
# ell
# A
# R_ring
# sigma_r_in
# sigma_r_out

# GaussianRing3D
# PA
# inc
# PA_ring
# ell
# J_0
# a_ring
# sigma
# h_z

# GaussianRingAz
# PA
# ell
# A_maj
# A_min_rel
# R_ring
# sigma_r

# ModifiedKing
# PA
# ell
# I_0
# r_c
# r_t
# alpha

# ModifiedKing2
# PA
# ell
# I_0
# r_c
# c
# alpha

# GaussExpX
# PA
# ell
# I0
# h_inner
# n_along
# h_outer
# r_break
# n_across
# r_e_across
# asymm

# PolySkyPlane
# I_0
# m_x
# m_y
# m_x2
# m_y2
# m_xy
# m_x3
# m_y3
# m_x2y
# m_xy2

# SersicJet
# PA
# ell
# n
# I_e
# r_e
