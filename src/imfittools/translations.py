"""
A set of functions to translate between various imfit functions
"""

import numpy as np
from scipy import special
from .ImfitModel import ImfitFunction
from .ImfitModel import ImfitParameter


def base_copy(src_func, new_func_name):
    """
    A helper to initiate a new function with coordinates of another
    """
    new_func = ImfitFunction(new_func_name, int(src_func.uname.split(".")[1]),
                             tag=src_func.tag)
    if src_func.own_coords:
        new_func.add_parameter(src_func.get_par_by_name("X0"))
        new_func.add_parameter(src_func.get_par_by_name("Y0"))
        new_func.own_coords = True
    else:
        new_func.x_cen_shared = src_func.x_cen_shared
        new_func.y_cen_shared = src_func.y_cen_shared
    return new_func


def DblBknExp3D_to_BknExp3D(dbl_bkn_func, remove="outer"):
    """
    Translate DblBknExp3D function to BknExp3D by removing one of breaks
    remove can be 'outer' or 'inner'. If remove == outer, then the outer disk is
    removed, if 'inner' -- inner
    """
    bkn_func = base_copy(dbl_bkn_func, "BknExp3D")
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("PA"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("inc"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("J_0"))
    if remove == "outer":
        # Removing outer disk
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h1"))
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h2"))
        r_break = dbl_bkn_func.get_par_by_name("r_break1").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    elif remove == "inner":
        # Removing inner disk
        h1 = dbl_bkn_func.get_par_by_name("h2").copy()
        h1.name = "h1"
        bkn_func.add_parameter(h1)
        h2 = dbl_bkn_func.get_par_by_name("h3").copy()
        h2.name = "h2"
        bkn_func.add_parameter(h2)
        r_break = dbl_bkn_func.get_par_by_name("r_break2").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    else:
        # Remove middle disk
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h1"))
        h2 = dbl_bkn_func.get_par_by_name("h3").copy()
        h2.name = "h2"
        bkn_func.add_parameter(h2)
        r_break = dbl_bkn_func.get_par_by_name("r_break2").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("n"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("z_0"))
    return bkn_func


def BknExp3D_to_ExponentialDisk3D(bkn_func, remove="outer"):
    """
    Translate BknExp3D function to simple exponential by removing one of disks
    """
    exp_func = base_copy(bkn_func, "ExponentialDisk3D")
    exp_func.add_parameter(bkn_func.get_par_by_name("PA"))
    exp_func.add_parameter(bkn_func.get_par_by_name("inc"))
    exp_func.add_parameter(bkn_func.get_par_by_name("J_0"))
    if remove == "outer":
        # Remove outer disk
        h = bkn_func.get_par_by_name("h1").copy()
    else:
        h = bkn_func.get_par_by_name("h2").copy()
        # remove inner disk
    h.name = "h"
    exp_func.add_parameter(h)
    exp_func.add_parameter(bkn_func.get_par_by_name("n"))
    exp_func.add_parameter(bkn_func.get_par_by_name("z_0"))
    return exp_func


def BrokenExponential_to_Exponential(bkn_func, remove="outer"):
    """
    Translate BrokenExponential function to simple exponential by removing one of disks
    """
    exp_func = base_copy(bkn_func, "Exponential")
    exp_func.add_parameter(bkn_func.get_par_by_name("PA"))
    exp_func.add_parameter(bkn_func.get_par_by_name("ell"))
    exp_func.add_parameter(bkn_func.get_par_by_name("I_0"))
    if remove == "outer":
        # Remove outer disk
        h = bkn_func.get_par_by_name("h1").copy()
    else:
        h = bkn_func.get_par_by_name("h2").copy()
        # remove inner disk
    h.name = "h"
    exp_func.add_parameter(h)
    return exp_func


def ExponentialDisk3D_to_BknExp3D(exp_func, break_type="same"):
    """
    Translate ExponentialDisk3D function to BknExp3D by adding a break
    break_type: 'same' h_2 = h_1
                'up' h_2 = 2 * h_1
                'down' h_2 = 0.5 * h_1
    """
    bkn_func = base_copy(exp_func, "BknExp3D")
    bkn_func.add_parameter(exp_func.get_par_by_name("PA"))
    bkn_func.add_parameter(exp_func.get_par_by_name("inc"))
    bkn_func.add_parameter(exp_func.get_par_by_name("J_0"))
    h1 = exp_func.get_par_by_name("h").copy()
    h1.name = "h1"
    h2 = exp_func.get_par_by_name("h").copy()
    if break_type == "up":
        h2.value *= 2
    elif break_type == "down":
        h2.value *= 0.5
    h2.name = "h2"
    bkn_func.add_parameter(h1)
    bkn_func.add_parameter(h2)
    r_break = ImfitParameter("r_break", value=h1.value*3, lowerLim=h1.value, upperLim=h1.value*6, fixed=False)
    bkn_func.add_parameter(r_break)
    bkn_func.add_parameter(exp_func.get_par_by_name("n"))
    bkn_func.add_parameter(exp_func.get_par_by_name("z_0"))
    return bkn_func


def ExponentialDisk3D_to_Exponential(exp3d_func):
    """
    Translate 3D exponential to a simple exponential
    """
    exp_func = base_copy(exp3d_func, "Exponential")
    exp_func.add_parameter(exp3d_func.get_par_by_name("PA"))
    h_value = exp3d_func.get_par_by_name("h").value
    z0_value = exp3d_func.get_par_by_name("z_0").value
    ell_value = min(0.945, 1-z0_value/h_value)
    ell = ImfitParameter("ell", value=ell_value, lowerLim=0.0, upperLim=0.95, fixed=False)
    exp_func.add_parameter(ell)
    j0_value = exp3d_func.get_par_by_name("J_0").value
    mu0_value = j0_value * 2 * h_value
    mu0 = ImfitParameter("I_0", value=mu0_value, lowerLim=1.0, upperLim="inf", fixed=False)
    exp_func.add_parameter(mu0)
    exp_func.add_parameter(exp3d_func.get_par_by_name("h"))
    return exp_func


def BknExp3D_to_BrokenExponential(bkn3d_func):
    """
    Translate 3D broken exponential to a simple broken exponential
    """
    bkn_func = base_copy(bkn3d_func, "BrokenExponential")
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("PA"))
    h_value = (bkn3d_func.get_par_by_name("h1") + bkn3d_func.get_par_by_name("h2")) / 2
    z0_value = bkn3d_func.get_par_by_name("z0").value
    ell_value = min(0.945, 1-z0_value/h_value)
    ell = ImfitParameter("ell", value=ell_value, lowerLim=0.0, upperLim=0.95, fixed=False)
    bkn_func.add_parameter(ell)
    j0_value = bkn3d_func.get_par_by_name("J_0").value
    mu0_value = j0_value * 2 * h_value
    mu0 = ImfitParameter("I_0", value=mu0_value, lowerLim=1.0, upperLim="inf", fixed=False)
    bkn_func.add_parameter(mu0)
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("h1").value)
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("h2").value)
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("r_break").value)
    alpha = ImfitParameter("alpha", value=20, fixed=False)
    bkn_func.add_parameter(alpha)
    return bkn_func


def BknExp3D_to_BrokenExponential2(bkn3d_func):
    """
    Translate 3D broken exponential to a simple broken exponential without a smoothing parameter
    """
    bkn_func = base_copy(bkn3d_func, "BrokenExponential2")
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("PA"))
    h_value = (bkn3d_func.get_par_by_name("h1").value + bkn3d_func.get_par_by_name("h2").value) / 2
    z0_value = bkn3d_func.get_par_by_name("z_0").value
    p = z0_value/h_value
    incl = np.radians(bkn3d_func.get_par_by_name("inc").value)
    ell_value = min(0.945, 1-(np.cos(incl)**2 * (1-p**2) + p**2))
    ell = ImfitParameter("ell", value=ell_value, lowerLim=0.0, upperLim=0.95, fixed=False)
    bkn_func.add_parameter(ell)
    j0_value = bkn3d_func.get_par_by_name("J_0").value
    mu0_value = j0_value * 2 * h_value
    mu0 = ImfitParameter("I_0", value=mu0_value, lowerLim=1.0, upperLim="inf", fixed=False)
    bkn_func.add_parameter(mu0)
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("h1"))
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("h2"))
    bkn_func.add_parameter(bkn3d_func.get_par_by_name("r_break"))
    return bkn_func


def DblBknExp3D_to_DoubleBrokenExponential(dbl_bkn3d_func):
    """
    Translate 3D exponential function with two breaks to a simple double
    broken exponential function
    """
    dbl_bkn_func = base_copy(dbl_bkn3d_func, "DoubleBrokenExponential")
    dbl_bkn_func.add_parameter(dbl_bkn3d_func.get_par_by_name("PA"))
    h_value = np.median([dbl_bkn3d_func.get_par_by_name("h1").value, dbl_bkn3d_func.get_par_by_name("h2").value,
                         dbl_bkn3d_func.get_par_by_name("h3").value])
    z0_value = dbl_bkn3d_func.get_par_by_name("z_0").value
    p = z0_value/h_value
    incl = np.radians(dbl_bkn3d_func.get_par_by_name("inc").value)
    ell_value = min(0.945, 1-(np.cos(incl)**2 * (1-p**2) + p**2))
    ell = ImfitParameter("ell", value=ell_value, lowerLim=0.0, upperLim=0.95, fixed=False)
    dbl_bkn_func.add_parameter(ell)
    j0_value = dbl_bkn3d_func.get_par_by_name("J_0").value
    mu0_value = j0_value * 2 * h_value
    mu0 = ImfitParameter("I_0", value=mu0_value, lowerLim=1.0, upperLim="inf", fixed=False)
    dbl_bkn_func.add_parameter(mu0)
    dbl_bkn_func.add_parameter(dbl_bkn3d_func.get_par_by_name("h1"))
    dbl_bkn_func.add_parameter(dbl_bkn3d_func.get_par_by_name("h2"))
    dbl_bkn_func.add_parameter(dbl_bkn3d_func.get_par_by_name("h3"))
    dbl_bkn_func.add_parameter(dbl_bkn3d_func.get_par_by_name("r_break1"))
    dbl_bkn_func.add_parameter(dbl_bkn3d_func.get_par_by_name("r_break2"))
    return dbl_bkn_func


def DoubleBrokenExponential_to_BrokenExponential2(dbl_bkn_func, remove="outer"):
    """
    Translate DoubleBrokenExponential function to BrokenExponential2 by removing one of breaks.
    remove parameter can be 'outer' or 'inner'. If remove == outer, then the outer disk is
    removed, if 'inner' -- inner, 'middle' -- middle one
    """
    bkn_func = base_copy(dbl_bkn_func, "BrokenExponential2")
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("PA"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("ell"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("I_0"))
    if remove == "outer":
        # Removing outer disk
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h1"))
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h2"))
        r_break = dbl_bkn_func.get_par_by_name("r_break1").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    elif remove == "inner":
        # Removing inner disk
        h1 = dbl_bkn_func.get_par_by_name("h2").copy()
        h1.name = "h1"
        bkn_func.add_parameter(h1)
        h2 = dbl_bkn_func.get_par_by_name("h3").copy()
        h2.name = "h2"
        bkn_func.add_parameter(h2)
        r_break = dbl_bkn_func.get_par_by_name("r_break2").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    else:
        # Remove middle disk
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h1"))
        h2 = dbl_bkn_func.get_par_by_name("h3").copy()
        h2.name = "h2"
        bkn_func.add_parameter(h2)
        r_break = dbl_bkn_func.get_par_by_name("r_break2").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    return bkn_func


def BrokenExponential2_to_Exponential(bkn_func, remove="outer"):
    """
    Translate BknExp3D function to simple exponential by removing one of disks, 'inner' or 'outer'
    """
    exp_func = base_copy(bkn_func, "Exponential")
    exp_func.add_parameter(bkn_func.get_par_by_name("PA"))
    exp_func.add_parameter(bkn_func.get_par_by_name("ell"))
    exp_func.add_parameter(bkn_func.get_par_by_name("I_0"))
    if remove == "outer":
        # Remove outer disk
        h = bkn_func.get_par_by_name("h1").copy()
    else:
        h = bkn_func.get_par_by_name("h2").copy()
        # remove inner disk
    h.name = "h"
    exp_func.add_parameter(h)
    return exp_func


def BknEdgeOn_to_EdgeOnDisk(bkn_func, remove="outer"):
    """
    Translate BknEdgeOn function to simple edge on disk by removing one of disks, 'inner' or 'outer'
    """
    eon_func = base_copy(bkn_func, "EdgeOnDisk")
    eon_func.add_parameter(bkn_func.get_par_by_name("PA"))
    j_0 = bkn_func.get_par_by_name("J_0")
    j_0.name = "L_0"
    eon_func.add_parameter(j_0)
    if remove == "outer":
        # Remove outer disk
        h = bkn_func.get_par_by_name("h1").copy()
    else:
        h = bkn_func.get_par_by_name("h2").copy()
        # remove inner disk
    h.name = "h"
    eon_func.add_parameter(h)
    eon_func.add_parameter(bkn_func.get_par_by_name("n"))
    eon_func.add_parameter(bkn_func.get_par_by_name("z_0"))
    return bkn_func


def Exponential_to_BrokenExponential2(exp_func, break_type="same"):
    """
    Translate Exponential function to simple BrokenExponential2 by adding a break
    break_type: 'same' h_2 = h_1
                'up' h_2 = 2 * h_1
                'down' h_2 = 0.5 * h_1
    """
    bkn_func = base_copy(exp_func, "BrokenExponential2")
    bkn_func.add_parameter(exp_func.get_par_by_name("PA"))
    bkn_func.add_parameter(exp_func.get_par_by_name("ell"))
    bkn_func.add_parameter(exp_func.get_par_by_name("I_0"))
    h1 = exp_func.get_par_by_name("h").copy()
    h1.name = "h1"
    h2 = exp_func.get_par_by_name("h").copy()
    h2.name = "h2"
    if break_type == "up":
        h2.value *= 2
    elif break_type == "down":
        h2.value *= 0.5
    bkn_func.add_parameter(h1)
    bkn_func.add_parameter(h2)
    r_break = ImfitParameter("r_break", value=h1.value*3, lowerLim=h1.value, upperLim=h1.value*6, fixed=False)
    bkn_func.add_parameter(r_break)
    return bkn_func


def BknExp3D_to_BknEdgeOn(bkn3d_func):
    """
    Translate 3D broken exponential with the default integrator to a 3D model
    with the integrator based on Gauss-Legendre quadratures
    """
    new_func = base_copy(bkn3d_func, "BknEdgeOn")
    new_func.add_parameter(bkn3d_func.get_par_by_name("PA"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("J_0"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("h1"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("h2"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("r_break"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("n"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("z_0"))
    return new_func


def DblBknEdgeOn_to_BknEdgeOn(dbl_bkn_func, remove="outer"):
    """
    Translate DblBknEdgeOn function to BknEdgeOn by removing one of breaks.
    remove parameter can be 'outer' or 'inner'. If remove == outer, then the outer disk is
    removed, if 'inner' -- inner, 'middle' -- middle one
    """
    bkn_func = base_copy(dbl_bkn_func, "BknEdgeOn")
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("PA"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("J_0"))
    if remove == "outer":
        # Removing outer disk
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h1"))
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h2"))
        r_break = dbl_bkn_func.get_par_by_name("r_break_1").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    elif remove == "inner":
        # Removing inner disk
        h1 = dbl_bkn_func.get_par_by_name("h2").copy()
        h1.name = "h1"
        bkn_func.add_parameter(h1)
        h2 = dbl_bkn_func.get_par_by_name("h3").copy()
        h2.name = "h2"
        bkn_func.add_parameter(h2)
        r_break = dbl_bkn_func.get_par_by_name("r_break_2").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    else:
        # Remove middle disk
        bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("h1"))
        h2 = dbl_bkn_func.get_par_by_name("h3").copy()
        h2.name = "h2"
        bkn_func.add_parameter(h2)
        r_break = dbl_bkn_func.get_par_by_name("r_break_2").copy()
        r_break.name = "r_break"
        bkn_func.add_parameter(r_break)
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("n"))
    bkn_func.add_parameter(dbl_bkn_func.get_par_by_name("z_0"))
    return bkn_func


def DblBknExp3D_to_DblBknEdgeOn(bkn3d_func):
    """
    Translate 3D double broken exponential with the default integrator to a 3D model
    with the integrator based on Gauss-Legendre quadratures
    """
    new_func = base_copy(bkn3d_func, "DblBknEdgeOn")
    new_func.add_parameter(bkn3d_func.get_par_by_name("PA"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("J_0"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("h1"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("h2"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("h3"))
    r_break_1 = ImfitParameter("r_break_1",
                               value=bkn3d_func.get_par_by_name("r_break1").value,
                               lowerLim=bkn3d_func.get_par_by_name("r_break1").lowerLim,
                               upperLim=bkn3d_func.get_par_by_name("r_break1").upperLim,
                               fixed=False)
    new_func.add_parameter(r_break_1)
    r_break_2 = ImfitParameter("r_break_2",
                               value=bkn3d_func.get_par_by_name("r_break2").value,
                               lowerLim=bkn3d_func.get_par_by_name("r_break2").lowerLim,
                               upperLim=bkn3d_func.get_par_by_name("r_break2").upperLim,
                               fixed=False)
    new_func.add_parameter(r_break_2)
    new_func.add_parameter(bkn3d_func.get_par_by_name("n"))
    new_func.add_parameter(bkn3d_func.get_par_by_name("z_0"))
    return new_func


def ExponentialDisk3D_to_EdgeOnDisk(exp3d_func):
    """
    Translate 3D exponential that is edge on to a simple disk with a Bessel function
    """
    new_func = base_copy(exp3d_func, "EdgeOnDisk")
    new_func.add_parameter(exp3d_func.get_par_by_name("PA"))
    h_value = exp3d_func.get_par_by_name("h").value
    j0_value = exp3d_func.get_par_by_name("J_0").value
    L0_value = j0_value * 2 * h_value
    L0 = ImfitParameter("L_0", value=L0_value, lowerLim=0.0, upperLim="inf", fixed=False)
    new_func.add_parameter(L0)
    new_func.add_parameter(exp3d_func.get_par_by_name("h"))
    new_func.add_parameter(exp3d_func.get_par_by_name("n"))
    new_func.add_parameter(exp3d_func.get_par_by_name("z_0"))
    return new_func


def Sersic_to_PointSource(sersic_func):
    """
    Translates a Sersic function to a PointSource function (for example if the former is too small
    to be fitted properly)
    """
    psf_func = base_copy(sersic_func, "PointSource")
    n_value = sersic_func.get_par_by_name("n").value
    i_e_value = sersic_func.get_par_by_name("I_e").value
    re_value = sersic_func.get_par_by_name("r_e").value
    nu_n = 2 * n_value - 1/3 + 4/(405*n_value)
    i_total = 2 * 3.14 * n_value / (nu_n**(2*n_value))
    i_total *= special.gamma(2*n_value) * i_e_value
    i_total *= np.exp(nu_n) * re_value ** 2.0
    psf_func.add_parameter(ImfitParameter("I_tot", value=i_total, lowerLim=0.5*i_total,
                                          upperLim=2*i_total, fixed=False))
    return psf_func


def DustyExp3DGalaxy_to_BulgeDisk(input_model):
    """
    Translates a DustyExp3DGalaxy into *two* models: exponential disk + Sersic bulge. Dusty part is discarded.
    Returns Sersic and ExponentialDisk3D model functions
    """
    disk = base_copy(input_model, "ExponentialDisk3D")
    disk.add_parameter(input_model.get_par_by_name("PA"))
    disk.add_parameter(input_model.get_par_by_name("inc"))
    disk.add_parameter(input_model.get_par_by_name("J_0"))
    disk.add_parameter(input_model.get_par_by_name("h"))
    disk.add_parameter(input_model.get_par_by_name("n_disk"))
    disk.add_parameter(input_model.get_par_by_name("z_0"))
    bulge = base_copy(input_model, "Sersic")
    bulge.add_parameter(input_model.get_par_by_name("PA"))
    bulge.add_parameter(input_model.get_par_by_name("ell"))
    bulge.add_parameter(input_model.get_par_by_name("n_bulge"))
    bulge.add_parameter(input_model.get_par_by_name("I_e"))
    bulge.add_parameter(input_model.get_par_by_name("r_e"))
    return bulge, disk
