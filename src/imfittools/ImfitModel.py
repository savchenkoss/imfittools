#! /usr/bin/env python

import subprocess
import numpy as np
import os
from . import fixers
from .param_ranges import param_ranges


def parse_imfit_line(line):
    """ Function parses line of imfit data file and
    returns parameters"""
    params = line.split()
    if "+/-" in line:
        # This is a line with parameter error estimation
        name = line.split()[0]
        value = float(params[1])
        error = float(params[4])
        lowerLim = upperLim = None
        fixed = False
        return ImfitParameter(name, value, lowerLim, upperLim, fixed, error)
    # We can drop the comment by now
    if "#" in line:
        line = line[:line.index("#")].strip()
        params = line.split()
    if (len(params) >= 3) and ("," in params[2]):
        # This is a line with parameter ranges
        name = params[0]
        value = float(params[1])
        rangeParams = params[2].split(",")
        lowerLim = float(rangeParams[0])
        if rangeParams[1] != "inf":
            upperLim = float(rangeParams[1])
        else:
            upperLim = np.inf
        fixed = False
        return ImfitParameter(name, value, lowerLim, upperLim, fixed, error=None)

    if (len(params) >= 3) and (params[2] == "fixed"):
        # This is a line with a fixed parameter value
        name = params[0]
        value = float(params[1])
        error = lowerLim = upperLim = None
        fixed = True
        return ImfitParameter(name, value, lowerLim, upperLim, fixed)

    if (len(params)) == 2:
        # No errors and no ranges given, not even information if this parameter fixed
        error = lowerLim = upperLim = None
        fixed = False
        name = params[0]
        value = float(params[1])
        return ImfitParameter(name, value, lowerLim, upperLim, fixed, error)
    # If we got here, there is some unsupported line structure
    return None


class ImfitParameter(object):
    """ Just a container of parameter instance:
    parameter name, value and its range"""
    def __init__(self, name, value, lowerLim=None, upperLim=None, fixed=False, error=None):
        self.name = name
        self.value = value
        self.lowerLim = lowerLim
        self.upperLim = upperLim
        self.badBoundary = False
        self.fixed = fixed
        self.error = error

    def __repr__(self):
        out_str = f"name={self.name} value={self.value}"
        if self.error:
            out_str += f" +/- {self.error}"
        if self.lowerLim:
            out_str += f" low={self.lowerLim} high={self.upperLim}"
        if self.fixed:
            out_str += " fixed"
        return out_str

    def __mul__(self, other):
        if isinstance(other, (int, float)):
            # Multiplication by a number
            new_value = self.value * other
            new_lowerLim = self.lowerLim * other if self.lowerLim is not None else None
            new_upperLim = self.upperLim * other if self.upperLim is not None else None
            return ImfitParameter(name=self.name, value=new_value, lowerLim=new_lowerLim,
                                  upperLim=new_upperLim, fixed=self.fixed, error=self.error)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        if isinstance(other, (int, float)):
            # Division by a number
            new_value = self.value / other
            new_lowerLim = self.lowerLim / other if self.lowerLim is not None else None
            new_upperLim = self.upperLim / other if self.upperLim is not None else None
            return ImfitParameter(name=self.name, value=new_value, lowerLim=new_lowerLim,
                                  upperLim=new_upperLim, fixed=self.fixed, error=self.error)

    def __add__(self, other):
        if isinstance(other, (int, float)):
            # Add number to a parameter
            new_value = self.value + other
            new_lowerLim = self.lowerLim + other if self.lowerLim is not None else None
            new_upperLim = self.upperLim + other if self.upperLim is not None else None
            return ImfitParameter(name=self.name, value=new_value, lowerLim=new_lowerLim,
                                  upperLim=new_upperLim, fixed=self.fixed, error=self.error)

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if isinstance(other, (int, float)):
            # Subtract number from a parameter
            new_value = self.value - other
            new_lowerLim = self.lowerLim - other if self.lowerLim is not None else None
            new_upperLim = self.upperLim - other if self.upperLim is not None else None
            return ImfitParameter(name=self.name, value=new_value, lowerLim=new_lowerLim,
                                  upperLim=new_upperLim, fixed=self.fixed, error=self.error)

    def copy(self):
        return ImfitParameter(name=self.name, value=self.value, lowerLim=self.lowerLim,
                              upperLim=self.upperLim, fixed=self.fixed, error=self.error)

    def tostring(self, fixAll):
        width = max(len(self.name)+3, 12)
        if self.badBoundary:
            comment = "   # bad boundary"
        else:
            comment = ""
        if fixAll or self.fixed:
            # Fixed parameter
            return f"{self.name:{width}} {self.value:6.5f}     fixed\n"
        elif (self.fixed is False) and (self.lowerLim is None):
            # Not fixed, but without borders
            return f"{self.name:{width}} {self.value:6.5f}\n"
        else:
            # Not fixed with borders specified
            if self.upperLim != "inf":
                return f"{self.name:{width}} {self.value:6.5f} {self.lowerLim:12.5f},{self.upperLim:1.5f} {comment}\n"
            else:
                return f"{self.name:{width}} {self.value:6.5f} {self.lowerLim:12.5f},inf {comment}\n"

    def change_value(self, newValue):
        self.value = newValue
        if not self.fixed:
            if self.lowerLim > newValue:
                self.lowerLim = newValue - newValue*0.001
            if self.upperLim < newValue:
                self.upperLim = newValue + newValue*0.001


class ImfitFunction:
    """ Class represents imfit function with
    all its parameters their ranges"""
    def __init__(self, funcName, ident, tag=None):
        # ident is a unique number of the function
        # funcName is just a type of the function and it can be
        # the same for different galaxy components
        # tag is a commented word after function name like  Sersic # bulge
        self.name = funcName
        self.ident = ident
        self.uname = f"{funcName}.{ident}"  # func unique name
        if tag is None:
            self.tag = ""
        else:
            self.tag = tag
        self.params = []
        self.own_coords = False  # True if function has own coordinates in config
        # If the function does not have coordinates in the config, it means that it
        # shares the coordinates with the previous function
        self.x_cen_shared = None
        self.y_cen_shared = None

    def copy(self):
        """
        Make a copy of a function
        """
        copied_function = ImfitFunction(self.name, self.ident, tag=self.tag)
        for param in self.params:
            copied_function.params.append(param.copy())
        copied_function.uname = f"{self.name}.{self.ident}"
        copied_function.own_coords = self.own_coords
        copied_function.x_cen_shared = self.x_cen_shared
        copied_function.y_cen_shared = self.y_cen_shared
        return copied_function

    def to_input_file(self, xcen=None, ycen=None, file_name=None, fixAll=False):
        """
        Create an input file with only this function
        """
        with open(file_name, "w") as fout:
            # Write comment lines at first
            if (xcen is not None) and (ycen is not None):
                fout.write(f"X0  {xcen}\nY0 {ycen}\n")
            elif self.own_coords:
                fout.write(self.get_par_by_name("X0").tostring(fixAll))
                fout.write(self.get_par_by_name("Y0").tostring(fixAll))
            else:
                fout.write("X0  500\n Y0 500\n")
            fout.write("FUNCTION " + self.name+"\n")
            for par in self.params:
                if par.name not in ("X0", "Y0"):
                    fout.write(par.tostring(fixAll))
        # print "Model was saved to '%s'\n" % (fileName)
        return file_name

    def get_total_flux(self):
        """
        Return fluxes of all components as a fraction of the total flux
        """
        self.to_input_file(file_name="/tmp/imfit_model_tmp.imfit")
        call_str = "makeimage /tmp/imfit_model_tmp.imfit --nosave --print-fluxes >/tmp/imfit_fluxes_tmp.dat"
        subprocess.call(call_str, shell=True)
        fraction_started = False
        for line in open("/tmp/imfit_fluxes_tmp.dat"):
            if line.startswith("#"):
                continue
            if (fraction_started is False) and ("Component" not in line):
                continue
            else:
                fraction_started = True
            if " --- " in line and (not line.startswith("Total")):
                return float(line.split()[1])

    def add_parameter(self, newParameter):
        self.params.append(newParameter)

    def num_of_params(self):
        return len(self.params)

    def get_par_by_name(self, name):
        for par in self.params:
            if par.name == name:
                return par
        return None

    def get_size(self):
        """ Return a parameter that corresponds to the characteristic size of the component """
        if self.name in ("Exponential", "ExponentialDisk3D", "EdgeOnDisk", "Exponential_GenEllipse"):
            return self.get_par_by_name("h").value
        if self.name in ("Sersic", "Core-Sersic", "Sersic_GenEllipse"):
            return self.get_par_by_name("r_e").value
        if self.name in ("BrokenExponential2D", "BrokenExponentialDisk3D", "BknExp3D",
                         "BrokenExponential", "BrokenExponential2"):
            return 0.5 * (self.get_par_by_name("h1").value + self.get_par_by_name("h2").value)
        if self.name in ("DblBknExp3D", "DoubleBrokenExponential"):
            return (self.get_par_by_name("h1").value + self.get_par_by_name("h2").value +
                    self.get_par_by_name("h3").value) / 3
        return None

    def get_mu0(self):
        """
        Returns a surface brightness at the center of the component
        """
        comps = ("Exponential", "Exponential_GenEllipse", "BrokenExponential", "BrokenExponential2",
                 "BrokenExponential2D", "DoubleBrokenExponential", "FerrersBar2D", "FlatBar",
                 "Gaussian", "Moffat")
        if self.name in comps:
            return self.get_par_by_name("I_0").value
        if self.name in ("BknExp3D", "BrokenExponentialDisk3D", "BrokenExponentialDisk3DEdgeOn"):
            h = (self.get_par_by_name("h1").value + self.get_par_by_name("h2").value) / 2
            return 2 * self.get_par_by_name("J_0").value * h
        if self.name in ("DblBknExp3D", "DoubleBrokenExponentialDisk3D", "DoubleBrokenExponentialDisk3DEdgeOn"):
            h = (self.get_par_by_name("h1").value + self.get_par_by_name("h2").value +
                 self.get_par_by_name("h3").value) / 3
            return 2 * self.get_par_by_name("J_0").value * h
        if self.name == "Sersic":
            n = self.get_par_by_name("n").value
            nu = 2*n - 1/3
            return self.get_par_by_name("I_e").value * np.exp(nu)
        return None

    def get_center(self):
        """
        Returns function coordinates, own or shared with another function
        """
        if self.own_coords is True:
            return self.get_par_by_name("X0").value, self.get_par_by_name("Y0").value
        else:
            return self.x_cen_shared, self.y_cen_shared

    def get_marks(self, axis):
        """
        Returns a list of special locations on the model (such as breals etc) in order to
        mark them on the plot.
        axis can be major or minor
        """
        marks = {}
        if self.name in ("Sersic", "Core-Sersic"):
            if axis == "major":
                marks["$r_e$"] = self.get_par_by_name("r_e").value
            elif axis == "minor":
                marks["$r_e$"] = self.get_par_by_name("r_e").value * (1-self.get_par_by_name("ell").value)
            return marks

        if self.name in ("BknExp3D", "BrokenExponentialDisk3D"):
            if axis == "major":
                marks["$r_{b}$"] = self.get_par_by_name("r_break").value
            elif axis == "minor":
                marks["$z_0$"] = self.get_par_by_name("r_break").value
            return marks

        if self.name in ("BrokenExponential", "BrokenExponential2"):
            if axis == "major":
                marks["$r_{b}$"] = self.get_par_by_name("r_break").value
            elif axis == "minor":
                marks["$r_{b}$"] = self.get_par_by_name("r_break").value * (1-self.get_par_by_name("ell").value)
            return marks

        if self.name == "BrokenExponential2D":
            if axis == "major":
                marks["$r_{b}$"] = self.get_par_by_name("r_break").value
            elif axis == "minor":
                marks["$z_{0}$"] = self.get_par_by_name("h_z").value
            return marks

        if self.name == "DblBknExp3D":
            if axis == "major":
                marks["$r_{b}^1$"] = self.get_par_by_name("r_break1").value
                marks["$r_{b}^2$"] = self.get_par_by_name("r_break2").value
            elif axis == "minor":
                marks["$z_0$"] = self.get_par_by_name("z_0").value
            return marks

        if self.name == "DoubleBrokenExponential":
            if axis == "major":
                marks["$r_{b}^1$"] = self.get_par_by_name("r_break1").value
                marks["$r_{b}^2$"] = self.get_par_by_name("r_break2").value
            elif axis == "minor":
                marks["$r_{b}^1$"] = self.get_par_by_name("r_break1").value * (1-self.get_par_by_name("ell").value)
                marks["$r_{b}^2$"] = self.get_par_by_name("r_break2").value * (1-self.get_par_by_name("ell").value)
            return marks
        return marks

    def show(self):
        """
        Print out function parameters
        """
        print(f"FUNCTION {self.name}")
        for par in self.params:
            if par.name not in ("X0", "Y0"):
                print(par.tostring(False).strip())

    def get_number_of_free_parameters(self):
        """
        Returns the number of free parameters of the function
        """
        tot = 0
        for par in self.params:
            if par.fixed is False:
                tot += 1
        return tot

    def rescale_sizes(self, factor):
        """
        Rescale all size-related parameters by a given factor
        """
        cent_dens_params = {"BknExp3D": ["J_0"],
                            "BrokenExponentialDisk3D": ["J_0"],
                            "DblBknEdgeOn": ["J_0"],
                            "DblBknExp3D": ["J_0"],
                            "DustyExp3DGalaxy": ["J_0", "J_0_dust"]
                            }

        size_params = {"BknEdgeOn": ["h1", "h2", "r_break", "z_0"],
                       "BknExp3D": ["h1", "h2", "r_break", "z_0"],
                       "BrokenExponential": ["h1", "h2", "r_break"],
                       "BrokenExponential2": ["h1", "h2", "r_break"],
                       "BrokenExponential2D": ["h1", "h2", "r_break", "h_z"],
                       "BrokenExponentialDisk3D": ["J_0", "h1", "h2", "r_break", "z_0"],
                       "Core-Sersic": ["r_e", "r_b"],
                       "DblBknEdgeOn": ["h1", "h2", "h3", "r_break_1", "r_break_2", "z_0"],
                       "DblBknExp3D": ["h1", "h2", "h3", "r_break1", "r_break2", "z_0"],
                       "DoubleBrokenExponential": ["h1", "h2", "h3", "r_break1", "r_break2"],
                       "DustyExp3DGalaxy": ["r_e", "h", "z_0", "h_dust", "z_0_dust"],
                       "EdgeOnDisk": ["h", "z_0"],
                       "EdgeOnRing": ["r", "sigma_r", "sigma_z"],
                       "EdgeOnRing2Side": ["r", "sigma_r_in", "sigma_r_out", "sigma_z"],
                       "Exponential": ["h"],
                       "ExponentialDisk3D": ["h", "z_0"],
                       "Exponential_GenEllipse": ["h"],
                       "FerrersBar2D": ["a_bar"],
                       "Gaussian": ["sigma"],
                       "GaussianJet": ["sigma"],
                       "GaussianRing": ["A", "R_ring", "sigma_r"],
                       "GaussianRing2Side": ["A", "R_ring", "sigma_r_in", "sigma_r_out}"],
                       "Moffat": ["fwhm"],
                       "Sersic": ["r_e"],
                       "SersicJet": ["r_e"],
                       "Sersic_GenEllipse": ["r_e"]}

        if self.name not in size_params:
            raise NotImplementedError

        for par_name in size_params[self.name]:
            self.get_par_by_name(par_name).value *= factor
            if self.get_par_by_name(par_name).lowerLim is not None:
                self.get_par_by_name(par_name).lowerLim *= factor
                self.get_par_by_name(par_name).upperLim *= factor

        if self.name in cent_dens_params:
            for par_name in cent_dens_params[self.name]:
                self.get_par_by_name(par_name).value /= factor
                if self.get_par_by_name(par_name).lowerLim is not None:
                    self.get_par_by_name(par_name).lowerLim /= factor
                    self.get_par_by_name(par_name).upperLim /= factor

    def rescale_fluxes(self, factor):
        """
        Rescale flux by the given factor
        """
        flux_pars = ["I_0", "I_e", "J_0", "I_b", "L_0", "I0", "J0", "I_tot"]
        for par in self.params:
            if par.name in flux_pars:
                par.value *= factor
                if par.lowerLim is not None:
                    par.lowerLim *= factor
                    par.upperLim *= factor

    def rotate(self, angle):
        """
        Change position angle by a given value
        """
        if self.get_par_by_name("PA") is None:
            return
        self.get_par_by_name("PA").value += angle
        if self.get_par_by_name("PA").lowerLim is not None:
            self.get_par_by_name("PA").lowerLim += angle
            self.get_par_by_name("PA").upperLim += angle


class ImfitModel:
    """Imfit functions and their parameters"""
    def __init__(self, modelFileName=None):
        self.listOfFunctions = []
        self.additional_parameters = {}
        self.numberOfParams = 0
        self.comment_lines = []
        if modelFileName is None:
            return
        print("Reading '%s':" % (modelFileName))
        funcName = None
        currentFunction = None
        ident = -1
        has_coords = False
        x0 = None
        y0 = None
        for line in open(modelFileName):
            sLine = line.strip()
            if sLine.startswith("#"):
                # It is a comment line, just skip it
                self.comment_lines.append(line)
                continue
            if len(sLine) == 0:
                # Empty line
                continue
            if sLine.startswith("GAIN"):
                self.additional_parameters["GAIN"] = float(line.split()[1])
                continue
            if sLine.startswith("READNOISE"):
                self.additional_parameters["READNOISE"] = float(line.split()[1])
                continue
            if sLine.startswith("X0"):
                x0 = parse_imfit_line(sLine)
                has_coords = True
            elif sLine.startswith("Y0"):
                y0 = parse_imfit_line(sLine)
            elif sLine.startswith("FUNCTION"):
                # New function is found.
                ident += 1
                if '#' in line:
                    tag = line.split('#')[1].strip()
                else:
                    tag = None
                # If we are working already with some function, then
                # the list of parameters for this function is over and we can
                # add it to the function list
                if funcName is not None:
                    if currentFunction.own_coords is False:
                        if self.listOfFunctions:
                            x, y = self.listOfFunctions[-1].get_center()
                            currentFunction.x_cen_shared, currentFunction.y_cen_shared = x, y
                    self.listOfFunctions.append(currentFunction)
                funcName = sLine.split()[1]
                currentFunction = ImfitFunction(funcName, ident, tag)
                if x0 is not None:
                    currentFunction.add_parameter(x0)
                    currentFunction.add_parameter(y0)
                currentFunction.own_coords = has_coords
                has_coords = False
                self.numberOfParams += 2
            else:
                # If line does not contain nor coordinates nor function name
                # then in has to be a parameter line
                param = parse_imfit_line(sLine)
                currentFunction.add_parameter(param)
                self.numberOfParams += 1
        # We are finished with parsing this function block. If we do not have
        # coordinates at this point, this means that this function shares them
        # with a previous one, so let's store them
        if currentFunction.own_coords is False:
            currentFunction.x_cen_shared, currentFunction.y_cen_shared = self.listOfFunctions[-1].get_center()
        # append the last function
        self.listOfFunctions.append(currentFunction)
        # Print some statistics
        print("  %i functions found (%i parameters)\n" % (len(self.listOfFunctions), self.numberOfParams))

    def copy(self):
        """
        Make a copy of a model
        """
        copy_model = ImfitModel(None)
        for func in self.listOfFunctions:
            copy_model.listOfFunctions.append(func.copy())
        copy_model.additional_parameters = self.additional_parameters.copy()
        copy_model.numberOfParams = self.numberOfParams
        copy_model.comment_lines = self.comment_lines.copy()
        return copy_model

    def randomize(self):
        """
        Vary all parameters that have error estimations by gaussian with sigma=error. Returns a
        copy of a model, do not alter this object
        """
        copy_model = self.copy()
        for func in copy_model.listOfFunctions:
            for param in func.params:
                if param.error is not None:
                    # Randomize the value
                    new_value = param.value + np.random.normal(0.0, scale=param.error)
                    # If the boundaries are specified, check make sure that the new value is inside them
                    if (param.lowerLim is not None) and (param.upperLim is not None):
                        if new_value < param.lowerLim:
                            new_value = param.lowerLim
                        elif new_value > param.upperLim:
                            new_value = param.upperLim
                    # For X0 Y0 we dont have standart ranges
                    elif param.name in ("X0", "Y0"):
                        pass
                    # If the ranges are not specified, but there are default ranges of for this parameter
                    # check them instead
                    elif func.name in param_ranges:
                        lowerLim, upperLim = param_ranges[func.name][param.name]
                        if (lowerLim is not None) and (new_value < lowerLim):
                            new_value = lowerLim
                        elif (upperLim is not None) and (new_value > upperLim):
                            new_value = upperLim
                    else:
                        print(f"Function {func.name} do not have default parameter ranges.")
                        raise NotImplementedError
                    print(param)
                    param.value = new_value
                    print(param)
        return copy_model

    def add_comment(self, comment):
        self.comment_lines.append(f"# {comment.strip()}\n")

    def get_func_by_uname(self, uname):
        for func in self.listOfFunctions:
            if uname == func.uname:
                return func

    def create_input_file(self, fileName=None, fixAll=False):
        fout = open(fileName, "w")
        # Write comment lines at first
        fout.writelines(self.comment_lines)
        for name, value in self.additional_parameters.items():
            fout.write(f"{name} {value}\n")
        for func in self.listOfFunctions:
            if func.own_coords:
                fout.write(func.get_par_by_name("X0").tostring(fixAll))
                fout.write(func.get_par_by_name("Y0").tostring(fixAll))
            fout.write("FUNCTION " + func.name+"\n")
            for par in func.params:
                if par.name not in ("X0", "Y0"):
                    fout.write(par.tostring(fixAll))
        fout.close()
        # print "Model was saved to '%s'\n" % (fileName)
        return fileName

    def to_fits(self, fitsname, ncols=None, nrows=None):
        """
        Create a fits image with the model
        """
        if (ncols is None) or (nrows is None):
            x_cen, y_cen = self.get_biggest_component().get_center()
            ncols = int(x_cen * 2)
            nrows = int(y_cen * 2)
        self.create_input_file("/tmp/tmp_config_imfit.imfit")
        call_str = f"makeimage /tmp/tmp_config_imfit.imfit --ncols {ncols} --nrows {nrows} -o {fitsname}"
        subprocess.call(call_str, shell=True)
        os.remove("/tmp/tmp_config_imfit.imfit")

    def check_boundaries(self):
        """ Method takes other model object and checks if its parameter
        values are close to the parameter limiths of the self model"""
        badParams = []
        for selfFunc in self.listOfFunctions:
            for selfParam in selfFunc.params:
                if selfParam.fixed or (self.lowerLim is None):
                    # Fixed parameter, so it does not have boundaries. Nothing to check.
                    continue
                parRange = selfParam.upperLim - selfParam.lowerLim
                eps = parRange / 1000.0
                if (abs(selfParam.value-selfParam.upperLim) < eps) or (abs(selfParam.value-selfParam.lowerLim) < eps):
                    badParams.append("%s: %s" % (selfFunc.name, selfParam.name))
                    selfParam.badBoundary = True
        return badParams

    def copy_boundaries(self, rhsModel):
        """ Method copies boundaries of parameters from rhsNodel to
        the current one """
        for rhsFunc in rhsModel.listOfFunctions:
            selfFunc = self.get_func_by_uname(rhsFunc.uname)
            for rhsParam in rhsFunc.params:
                selfParam = selfFunc.get_par_by_name(rhsParam.name)
                if rhsParam.fixed:
                    # Fixed parameter, so it does not have boundaries.
                    selfParam.fixed = True
                else:
                    selfParam.lowerLim = min(rhsParam.lowerLim, selfParam.value)
                    selfParam.upperLim = max(rhsParam.upperLim, selfParam.value)

    def copy_add_params(self, rhsModel):
        self.additional_parameters = rhsModel.additional_parameters.copy()

    def get_funcs_by_class(self, func_class):
        return [func for func in self.listOfFunctions if func_class == func.name]

    def fix_all_funcs(self):
        for func in self.listOfFunctions:
            if func.uname.split(".")[0] in fixers.fixers_list:
                fixers.fixers_list[func.uname.split(".")[0]](func)
                for par in func.params:
                    if par.fixed or (par.lowerLim is False) or (par.upperLim is False):
                        continue
                    if par.value < par.lowerLim:
                        print(f"Warning: {par.name} value {par.value} is below lower limit {par.lowerLim}")
                        print("replacing it with lower limit")
                        par.value = par.lowerLim
                    if (par.upperLim != "inf") and (par.value > par.upperLim):
                        print(f"Warning: {par.name} value {par.value} is above upper limit {par.upperLim}")
                        print("replacing it with upper limit")
                        par.value = par.upperLim

    def recenter_all_components(self, x_new, y_new, dx, dy):
        """
        Function sets new coordinates of the center for all components
        """
        for func in self.listOfFunctions:
            if func.own_coords:
                func.get_par_by_name("X0").value = x_new
                if not func.get_par_by_name("X0").fixed:
                    func.get_par_by_name("X0").lowerLim = x_new-dx
                    func.get_par_by_name("X0").upperLim = x_new+dx
                func.get_par_by_name("Y0").value = y_new
                if not func.get_par_by_name("Y0").fixed:
                    func.get_par_by_name("Y0").lowerLim = y_new-dy
                    func.get_par_by_name("Y0").upperLim = y_new+dy
            else:
                func.x_cen_shared = x_new
                func.y_cen_shared = y_new

    def set_common_center(self, xcen: float, ycen: float, dx: float, dy: float):
        """
        Make such that all the function have the same coordinates of the center. In the imfit
        config it works like this: the first function has its own coordinates, and writes ther into
        the config file, while others do not own their coordinates such that they have to share them
        with the previous function
        """
        # Set coordinates for the first function
        first_function = self.listOfFunctions[0]
        if first_function.own_coords:
            x = first_function.get_par_by_name("X0")
            x.value = xcen
            x.lowerLim = xcen - dx
            x.upperLim = xcen + dx
            y = first_function.get_par_by_name("Y0")
            y.value = ycen
            y.lowerLim = ycen - dy
            y.upperLim = ycen + dy
        else:
            x = ImfitParameter("X0", value=xcen, lowerLim=xcen-dx, upperLim=xcen+dx)
            y = ImfitParameter("Y0", value=ycen, lowerLim=ycen-dy, upperLim=ycen+dy)
            self.params.insert(y)
            self.params.insert(x)
            self.own_coords = True
        # Remove coordinates from other functions
        for func in self.listOfFunctions[1:]:
            func.own_coords = False
            func.x_cen_shared = xcen
            func.y_cen_shared = ycen

    def set_to_all(self, parname, new_value, lowerLim=None, upperLim=None):
        for func in self.listOfFunctions:
            if func.get_par_by_name(parname) is not None:
                func.get_par_by_name(parname).value = new_value
                if (not func.get_par_by_name(parname).fixed) and (lowerLim is not None):
                    func.get_par_by_name(parname).lowerLim = lowerLim
                if (not func.get_par_by_name(parname).fixed) and (upperLim is not None):
                    func.get_par_by_name(parname).upperLim = upperLim

    def get_biggest_component(self):
        """
        Function returns the component that has largest characteristic size
        """
        sizes = np.array([component.get_size() for component in self.listOfFunctions], dtype=float)
        return self.listOfFunctions[np.argsort(1/sizes)[0]]

    def get_relative_fluxes(self):
        """
        Return fluxes of all components as a fraction of the total flux
        """
        self.create_input_file("/tmp/imfit_model_tmp.imfit")
        call_str = "makeimage /tmp/imfit_model_tmp.imfit --nosave --print-fluxes >/tmp/imfit_fluxes_tmp.dat"
        subprocess.call(call_str, shell=True)
        fluxes = []
        fraction_started = False
        for line in open("/tmp/imfit_fluxes_tmp.dat"):
            if line.startswith("#"):
                continue
            if (fraction_started is False) and ("Component" not in line):
                continue
            else:
                fraction_started = True
            if " --- " in line and (not line.startswith("Total")):
                fluxes.append(float(line.split()[3]))
        return fluxes

    def get_total_fluxes(self):
        """
        Return total fluxes of all components as in counts
        """
        self.create_input_file("/tmp/imfit_model_tmp.imfit")
        call_str = "makeimage /tmp/imfit_model_tmp.imfit --nosave --print-fluxes >/tmp/imfit_fluxes_tmp.dat"
        subprocess.call(call_str, shell=True)
        fluxes = []
        fraction_started = False
        for line in open("/tmp/imfit_fluxes_tmp.dat"):
            if line.startswith("#"):
                continue
            if (fraction_started is False) and ("Component" not in line):
                continue
            else:
                fraction_started = True
            if " --- " in line and (not line.startswith("Total")):
                fluxes.append(float(line.split()[1]))
        return fluxes

    def get_number_of_free_parameters(self):
        """
        Returns the number of total free parameters of all functions
        """
        tot = 0
        for func in self.listOfFunctions:
            tot += func.get_number_of_free_parameters()
        return tot

    def rescale_sizes(self, factor):
        """
        Rescale all functions sizes by a given factor
        """
        for func in self.listOfFunctions:
            func.rescale_sizes(factor)

    def rescale_fluxes(self, factor):
        """
        Rescale all functions fluxes by a given factor
        """
        for func in self.listOfFunctions:
            func.rescale_fluxes(factor)

    def rotate(self, angle):
        """
        Rotate all functions by a given factor
        """
        for func in self.listOfFunctions:
            func.rotate(angle)


class ImfitResult(ImfitModel):
    """
    The result file is almost identical, but does not have parameter ranges and does have BIC and AIC values
    """
    def __init__(self, file_name):
        super().__init__(file_name)
        for line in open(file_name):
            if line.startswith("#") and ("AIC:" in line):
                self.aic = float(line.split()[2])
                continue
            if line.startswith("#") and ("BIC:" in line):
                self.bic = float(line.split()[2])
                continue
            if line.startswith("#") and ("Best-fit value:" in line):
                self.chi = float(line.split()[3])
                continue
            if line.startswith("#") and ("Reduced value:" in line):
                self.chi_reduced = float(line.split()[3])
                continue


def compute_average_model(list_of_models, method="mean"):
    """
    Function gets a list of models (with the same function list) as an input and
    computes a model with average parameters as an output.
    method can be "mean" (default) of "median" to invoke an appropriate averaging function.
    """
    # A list should contain two or more models
    if len(list_of_models) < 2:
        raise ValueError("A list of models should contain at least two of them")
    # Check if all the models do have the same list of functions
    func_combinations = []
    for model in list_of_models:
        funcs = sorted([func.name for func in model.listOfFunctions])
        if funcs not in func_combinations:
            func_combinations.append(funcs)
    if len(func_combinations) > 1:
        msg = "All models should contain the same list of functions, but the following combination\n"
        msg += f"{func_combinations}\n"
        msg += "was found"
        raise ValueError(msg)
    # All functions in the model should be unique, otherwise it os not clear
    # which components should be averaged between the models
    if len(func_combinations[0]) != len(set(func_combinations[0])):
        raise ValueError("All functions in the model should be of different type")

    mean_model = list_of_models[0].copy()
    for func in mean_model.listOfFunctions:
        for param in func.params:
            values = [model.get_funcs_by_class(func.name)[0].get_par_by_name(param.name) for model in list_of_models]
            if method == "mean":
                param.value = np.mean(values)
            elif method == "median":
                param.value = np.median(values)
            else:
                raise ValueError("Method should be 'mean' or 'median'")
            if param.fixed is False:
                if param.lowerLim is not None:
                    lower_limits = []
                    for model in list_of_models:
                        lower = model.get_funcs_by_class(func.name)[0].get_par_by_name(param.name).lowerLim
                        if lower is not None:
                            lower_limits.append(lower)
                    if lower_limits:
                        param.lowerLim = min(lower_limits)
                if param.upperLim is not None:
                    upper_limits = []
                    for model in list_of_models:
                        upper = model.get_funcs_by_class(func.name)[0].get_par_by_name(param.name).upperLim
                        if upper is not None:
                            upper_limits.append(upper)
                    param.upperLim = max(upper_limits)
    return mean_model
