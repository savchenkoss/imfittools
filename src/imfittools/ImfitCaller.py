#! /usr/bin/env python

import subprocess
import time
from pathlib import Path
from collections import namedtuple
import os


FitStat = namedtuple("FitStat", ["chi_start", "chi_end", "iter_number", "aic_value", "bic_value", "reduced_chi_value"])


class ImfitCaller(object):
    """
    A class to wrap around imfit call
    """
    def __init__(self, binary=None):
        if binary is not None:
            # Specify non-default binary file
            self.binary = binary
        else:
            self.binary = "imfit"
        self.arguments = {"bbox": "", "logfile": "imfit_tmp_log.log",
                          "max-threads": os.cpu_count()}

    def setup(self, keys=None, **kwargs):
        """
        Specify imfit command line arguments. This function can be called several times to update the list.
        Additional to imfit default arguments are:
            bbox: a bounding box (in a form of string like '[405:700,844:1060]')
            logfile: save imfit output in a specified file
        """
        if keys is not None:
            for k in keys:
                if k.startswith("overpsf"):
                    self.arguments[k] = ""
                else:
                    self.arguments[k.replace("_", "-")] = ""
        for kw, value in kwargs.items():
            if kw.startswith("overpsf"):
                self.arguments[kw] = value
            else:
                self.arguments[kw.replace("_", "-")] = value

    def run(self, image_file):
        # Prepare decomposition command line
        call_str = f"{self.binary} {image_file}{self.arguments['bbox']} "
        for kw, value in self.arguments.items():
            if value is None:
                continue
            if kw in ("bbox", "logfile"):
                continue
            if kw in ("noise", "mask"):
                call_str += f"--{kw} {value}{self.arguments['bbox']} "
                continue
            call_str += f"--{kw} {value} "
        call_str += f" > {self.arguments['logfile']}"
        # Start the decomposition
        t_start = time.time()
        print("Starting decomposition")
        print(call_str)
        subprocess.call(call_str, shell=True)
        t_end = time.time()
        print(f"Decomposition finished in {t_end-t_start: 1.1f} seconds")
        # Check results
        # 1) Check log file
        iter_number = 0
        chi_start = None
        chi_end = None
        bic_value = None
        aic_value = None
        reduced_chi_value = None
        if Path(self.arguments['logfile']).exists():
            for line in open(self.arguments['logfile']):
                if line.startswith("#"):
                    continue
                if ("objective" in line) or ("fit statistic" in line):
                    if (len(line.split()) < 2) or (line.split()[-2] != "="):
                        continue
                    iter_number += 1
                    if chi_start is None:
                        chi_start = float(line.split()[-1])
                        chi_end = float(line.split()[-1])
                    else:
                        chi_end = float(line.split()[-1])
                if ("AIC" in line) and ("BIC" in line):
                    aic_value = float(line.split()[2][:-1])
                    bic_value = float(line.split()[5])
                    continue
                if ("Reduced Chi^2" in line) and ("equivalent" not in line):
                    reduced_chi_value = float(line.split()[3])
                    continue
                if ("Reduced Chi^2 equivalent" in line):
                    reduced_chi_value = float(line.split()[4])
                    continue
            return FitStat(chi_start=chi_start, chi_end=chi_end, iter_number=iter_number,
                           aic_value=aic_value, bic_value=bic_value, reduced_chi_value=reduced_chi_value)
        else:
            return None
